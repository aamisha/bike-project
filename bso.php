// project 1
<html>
<head>
<title>Bike Sharing Optimiser</title>
<meta charset="UTF-8">
<script>
window.onload = function () {

<?php
$old = ini_set('memory_limit', '512M');
$csvData = getCSVData();

$dataPoints1 = getPlanDurationDataPoints($csvData);
$dataPoints2 = getTripRouteCategoryDataPoints($csvData);
$dataPoints3 = getPassHolderDataPoints($csvData);
?>

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	title:{
		text: "Bike Sharing Optimiser"
	},
	axisX:{
		title: "Trip ID"
	},
	axisY:{
		title: "Plan Duration",
		titleFontColor: "#FF1111",
		lineColor: "#FF1111",
		labelFontColor: "#FF1111",
		tickColor: "#FF1111"
	},
	axisY2:{
		title: "Trip Route Category",
		titleFontColor: "#11FF11",
		lineColor: "#11FF11",
		labelFontColor: "#11FF11",
		tickColor: "#11FF11",
		includeZero: false
	},
	legend:{
		cursor: "pointer",
		dockInsidePlotArea: true,
		itemclick: toggleDataSeries
	},
	data: [{
		type: "line",
		name: "Plan Duration",
		markerSize: 0,
		toolTipContent: "Plan Duration",
		showInLegend: true,
		dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
	},{
		type: "line",
		axisYType: "secondary",
		name: "Trip Route Category",
		markerSize: 0,
		toolTipContent: "Trip Route Category",
		showInLegend: true,
		dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();

function toggleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else{
		e.dataSeries.visible = true;
	}
	chart.render();
}

}
</script>
</head>
<body>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<?php
    function getData($csvLine){
        $object = new stdClass();
        $object->tripId = $csvLine[0];
        $object->duration = $csvLine[1];
        $object->startingStationId = $csvLine[4];
        $object->startLat = (float) $csvLine[5];
        $object->startLng = (float) $csvLine[6];
        $object->endingStationId = $csvLine[7];
        $object->endLat = (float) $csvLine[8];
        $object->endLng = (float) $csvLine[9];
        $object->bikeId = $csvLine[10];
        $object->planDuration = $csvLine[11];
        $object->tripRouteCategory = $csvLine[12];
        $object->passHolderType = $csvLine[13];
        return $object;
    }
    //https://www.geodatasource.com/developers/php
    function distance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        if($miles > 0 && $miles < 10000) {
            return $miles;
        }
        return 0.0;
    }
    function getCSVData(){
        $i = 0;
        $csvData = array();
        $file = fopen("bso_data.csv","r");
        while(! feof($file)) {
            if($i == 0) {
                $headings = fgetcsv($file);
            } else {
                $csvData[] = getData(fgetcsv($file));
            }
            $i++;
        }

        fclose($file);

        return $csvData;
    }
    function getPopularStartStation($csvData){
        $count = array();
        foreach ($csvData as $object) {
            $existingCount = 0;
            if(array_key_exists($object->startingStationId, $count)){
                $existingCount = $count[$object->startingStationId];
            }
            $count[$object->startingStationId] = $existingCount + 1;
        }
        $id = '';
        $max = 0;
        foreach ($count as $key => $value) {
            if($value > $max) {
                $id = $key;
                $max = $value;
            }
        }
        return "Popular starting station id <strong>$id</strong>, travel count $max";
    }
    function getPopularEndStation($csvData){
        $count = array();
        foreach ($csvData as $object) {
            $existingCount = 0;
            if(array_key_exists($object->endingStationId, $count)){
                $existingCount = $count[$object->endingStationId];
            }
            $count[$object->endingStationId] = $existingCount + 1;
        }
        $id = '';
        $max = 0;
        foreach ($count as $key => $value) {
            if($value > $max) {
                $id = $key;
                $max = $value;
            }
        }
        return "Popular ending station id <strong>$id</strong>, travel count $max";
    }
    function getRegularCommute($csvData){
        $count = array();
        foreach ($csvData as $object) {
            $existingCount = 0;
            if(array_key_exists($object->bikeId, $count)){
                $existingCount = $count[$object->bikeId];
            }
            $count[$object->bikeId] = $existingCount + 1;
        }
        $daysCommuted = 100;
        $regularCommuters = array();
        foreach ($count as $key => $value) {
            if($value >= $daysCommuted) {
                $regularCommuters[] = $key;
            }
        }
        return "Regular bike sharing count <strong>" . count($regularCommuters) . "</strong>";
    }
    function getAverageDistance($csvData){
        $totalCommutes = count($csvData);

        $averageDistance = 0.0;
        foreach ($csvData as $object) {
            $averageDistance += distance($object->startLat, $object->startLng, $object->endLat, $object->endLng) / $totalCommutes;
        }
        return "Total Commutes $totalCommutes, average <strong>" . round($averageDistance, 3) . "</strong> miles";
    }
    function getPlanDurationDataPoints($csvData){
        $dataPoints = array();
        $i=0;
        foreach ($csvData as $object) {
            $i++;
            if ($i % 1000 != 0) continue;
            $dataPoints[] = array("x"=> $object->tripId, "y"=> $object->planDuration);
        }
        return $dataPoints;
    }
    function getTripRouteCategoryDataPoints($csvData){
        $dataPoints = array();
        $i=0;
        foreach ($csvData as $object) {
            $i++;
            if ($i % 1000 != 0) continue;
            $category = 0;
            if("Round Trip" === $object->tripRouteCategory) {
                $category = 100;
            } else if("One Way" === $object->tripRouteCategory) {
                $category = 200;
            }
            $dataPoints[] = array("x"=> $object->tripId, "y"=> $category);
        }
        return $dataPoints;
    }
    function getPassHolderDataPoints($csvData){
        $dataPoints = array();
        $i=0;
        foreach ($csvData as $object) {
            $i++;
            if ($i % 1000 != 0) continue;
            $type = 0;
            if("Monthly Pass" === $object->passHolderType) {
                $type = 100;
            } else if("Flex Pass" === $object->passHolderType) {
                $type = 200;
            } else if("Walk-up" === $object->passHolderType) {
                $type = 300;
            }
            $dataPoints[] = array("x"=> $object->tripId, "y"=> $type);
        }
        return $dataPoints;
    }

    $popularStart = getPopularStartStation($csvData);
    $popularEnd = getPopularEndStation($csvData);
    $averageDistance = getAverageDistance($csvData);
    $regularCommutes = getRegularCommute($csvData);

    echo "<p>";
    echo $popularStart . '<br/>';
    echo $popularEnd . '<br/>';
    echo $averageDistance . '<br/>';
    echo $regularCommutes . '<br/>';
    echo "</p>";

?>
</body>
</html>